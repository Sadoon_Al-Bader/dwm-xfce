# dwm-xfce4: Customized dwm to integrate with the Xfce4 desktop

## manpage is outdated, will look into it later. Check out config.h for keybindings and such


## Patches and features

- Integrates with xfce4-panel instead of dwm's top bar. Uses [anybar](https://dwm.suckless.org/patches/anybar/)
- New layouts: bstack, fibonacci, deck, centered master and more. All bound to keys `super+(shift+)t/y/u/i`.
- True fullscreen (`super+f`) and prevents focus shifting.
- Windows can be made sticky (`Super+Shift+s`).
- stacker: Move windows up the stack manually (`Super-Shift-Left/Right`).
- shiftview: Cycle through tags (`super+g/;`).
- vanitygaps: Gaps allowed across all layouts.
- swallow patch: if a program run from a terminal would make it inoperable, it temporarily takes its place to save space.
- tmux swallow patch: tmux is now also considered a terminal and can be swallowed. Thank you [soystemd](https://gist.github.com/soystemd/0ac35d048672c63d7fc3afb1fde4c5f2)
- tabbed swallows now, useful for vimb+tabbed and using my config (to be released) that bind Ctrl+n to open current page in alternate browser.
- Most windows that shouldn't tile don't. Includes xfce4-settings, timeshift-gtk, xfce4-taskmanager, gnome-calculator, file-roller, xfburn, and all "wrapper-2.0" instances (xfce4-panel plugins and such)

## Bugs and deficiencies

- Cannot know which tag you are on because dwm's bar is hidden, need to find a way to display that on xfce4-panel
- Clicking on the time to display the mini calendar view will cause windows to overlap with xfce4-panel, remedy with `xfce4-panel -r`
- `Mod+D` which opens up WhiskerMenu may not work unless your mouse is hovering over the location of the menu, use `Mod+Shift+D` to use the regular Application Launcher. 

